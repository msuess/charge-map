import React, {useEffect, useState, useRef, useCallback} from 'react';
import {GoogleMap, useLoadScript, Marker} from '@react-google-maps/api';
import debounce from 'lodash.debounce';

const DEFAULT_LOCATION = {
  lat: 52.520008,
  lng: 13.404954,
};

function Map() {
  const refMap = useRef(null);
  const {isLoaded, loadError} = useLoadScript({
    googleMapsApiKey: 'AIzaSyCWjqRKFoiBeeXFxzrF9HRZKrHgFx6DaWw',
  });
  const [chargerLocations, setChargerLocations] = useState([]);
  const [boundsLat, setBoundsLat] = useState(DEFAULT_LOCATION.lat);
  const [boundsLng, setBoundsLng] = useState(DEFAULT_LOCATION.lng);
  const onBoundsChanged = useCallback(() => {
    if (!refMap) {
      return;
    }
    const {center} = refMap.current.state.map;
    const lat = center.lat();
    const lng = center.lng();
    setBoundsLng(lng);
    setBoundsLat(lat);
  }, [refMap, setBoundsLat, setBoundsLng]);
  useEffect(() => {
    async function fetchData() {
      const response = await fetch(
        `https:api.openchargemap.io/v3/poi/?output=json&countrycode=DE&maxresults=10&compact=true&verbose=false&latitude=${boundsLat}&longitude=${boundsLng}&distance=10&distanceunit=KM`,
      );
      const json = await response.json();
      setChargerLocations(
        json.map(({AddressInfo: {Latitude, Longitude}}) => ({
          lat: Latitude,
          lng: Longitude,
        })),
      );
    }
    fetchData();
  }, [setChargerLocations, boundsLat, boundsLng]);
  const renderMap = () => {
    return (
      <GoogleMap
        id="charge-map"
        ref={refMap}
        mapContainerStyle={{
          height: '100vh',
          width: '100ve',
        }}
        zoom={15}
        center={{
          lat: boundsLat,
          lng: boundsLng,
        }}
        onBoundsChanged={debounce(onBoundsChanged, 500)}
      >
        {chargerLocations.map(({lat, lng}) => (
          <Marker key={`lat${lat},lng${lng}`} position={{lat, lng}} />
        ))}
      </GoogleMap>
    );
  };
  if (loadError) {
    return <div>Map cannot be loaded right now, sorry.</div>;
  }
  return isLoaded ? renderMap() : <div>Loading</div>;
}

function App() {
  return <Map />;
}

export default App;
